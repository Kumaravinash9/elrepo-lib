/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
import 'dart:convert';
import 'dart:io';

import 'package:elrepo_lib/repo.dart';
import 'package:elrepo_lib/retroshare.dart';
import 'package:eventsource/eventsource.dart';
import 'package:test/test.dart';
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/models.dart' as models;

class AUTH {
  static const String identityId = "bdbd397cf7800c5e085968e185633b50";
  static const String locationId = "814228577bc0c5da968c79272adcbfce";
  static const String passphrase = "test";
  static const String apiUser = "test";

  static initLocal() =>
      rs.initRetroshare(
          identityId: AUTH.identityId,
          locationId: AUTH.locationId,
          passphrase: AUTH.passphrase,
          apiUser: AUTH.apiUser
      );

  static initRemote() {
    setRetroshareServicePrefix("http://127.0.0.1:9091");
    rs.initRetroshare(
      identityId: "4b01ba1ab1ac85ef10cef012ea8b937e",
      locationId: AUTH.locationId,
      passphrase: "kkkk",
    );
  }
}

void main() {
  group('A group of tests', () {
//    Awesome awesome;
//
//    setUp(() {
//      awesome = Awesome();
//    });
//
//    test('First Test', () {
//      expect(awesome.isAwesome, isTrue);
//    });

    test("reverseProxy", () async {

      // Key given by use "/rsremote/:userName"
      String apiUser = "userTest", key  = "FFb938Lykp";

      // This lines are a copy of rsApiCall on RetroShare.dart
      String baseUrl = "http://localhost:8888/rsremote";
      String path  = "/rsGxsForums/getForumsSummaries";
//    String path = "/rsJsonApi/getAuthorizedTokens";
      final basicAuth = rs.makeAuthHeader(apiUser, key);
      Map<String, dynamic> params = {
        'forumId': "479328f21c718e6307d5bfbe0d8ac8a0"
      };

      rs.setRetroshareServicePrefix(baseUrl);
      final res = await rs.rsApiCall(
        path,
//        params: params,
        basicAuth: basicAuth,
      );

      print(res);
    });

    test("getAuthToken", () async =>
      expect(await repo.enableRemoteControl( "http://192.168.0.7:8888/rsremote", "testoftest"), true)
     );

    test("getContentPostsFromReferreds", () async {
      rs.initRetroshare(
          identityId: AUTH.identityId,
          locationId: AUTH.locationId,
          passphrase: AUTH.passphrase,
          apiUser: AUTH.apiUser
      );

//      var res = await repo.getContentPostsFromReferreds( await repo.getPostHeaders("BOOKMARKS", rs.authIdentityId));
      var res = await repo.getContentPostsFromReferreds( await repo.getPostHeaders("BOOKMARKS", "869a2f1ee6ea9f9e36999fd9544830ef"));

      print(res);
      print(res.length);
    }

    );


    test("getDownloadingBookmarks", () async {
      rs.initRetroshare(
          identityId: "869a2f1ee6ea9f9e36999fd9544830ef",
          locationId: AUTH.locationId,
          passphrase: "kkkk",
      );

      var downloaded = await repo.getBookmarks();
      var downloading = await repo.getBookmarks(downloading: true);
      var bookmarks = await getPostHeaders("BOOKMARKS", rs.authIdentityId);

      for (var post in bookmarks) print(post["mMsgName"]);
      print("bookmarks");
      print(bookmarks.length);


      for (var post in downloading) print(post["mMsgName"]);
      print("Downloading");
      print(downloading.length);

      for (var post in downloaded) print(post["mMsgName"]);
      print("Downloaded");
      print(downloaded.length);
    }

    );

    test("getPeers", () async {
      rs.initRetroshare(
        identityId: AUTH.identityId,
        locationId: AUTH.locationId,
        passphrase: AUTH.passphrase,
        apiUser: AUTH.apiUser
      );

      var res = await getSslPeerList();
      print(res);

//      res = await getSslPeerList(offline: true);
//      print(res);

    });

    test("doPromisquity", () async {
      setRetroshareServicePrefix("http://127.0.0.1:9091");
      rs.initRetroshare(
        identityId: "869a2f1ee6ea9f9e36999fd9544830ef",
        locationId: AUTH.locationId,
        passphrase: "kkkk",
      );

      var res = await doPromiscuity();
      print(res);
    });

    test("getExploreContent", () async {
      AUTH.initLocal();

      var res = await exploreContent();
      print("Content explored:");
      for(var post in res) {
        print( post["mAuthorId"]+ " - " +post["mMsgName"]);
      }

    });

    test("rsEvents", () async {
      AUTH.initLocal();

      await rs.RsEvents.registerEventsHandler(
        RsEventType.CHAT_MESSAGE,
        (event) {
          // Deserialize the message
//          var json = event.data != null ? jsonDecode(event.data) : null;
//          print(json);
//          if (json['event'] != null){
            print(json);
//          }
        },
        onError: (error) {
          print("ASSDA");
          print(error);
        }
      );
//      print("Reading events. Press enter to finish");
//      stdin.readLineSync();
      await Future.delayed(const Duration(seconds: 15));

    });

    test("distantSearch", () async {
      AUTH.initLocal();
//      AUTH.initRemote();

      var searchQuery = 'redpanal';
//      var searchId = await rs.RsGxsForum.distantSearchRequest(searchQuery);
      var searchId = await repo.distantSearch(searchQuery, (e) => print(e));
      print("Search Id:");
      print(searchId);
      await rs.RsEvents.registerEventsHandler(
          RsEventType.GXS_FORUMS,
              (Map<String, dynamic> event) {
            if (event['mForumEventCode'] == rs.RsForumEventCode.DISTANT_SEARCH_RESULT.index
                && event['mSearchId'] == searchId){
              print(event["mSearchResults"]);
            }
          },
          onError: (error) {
            print("onError callback");
            print(error);
          }
      );

      await Future.delayed(const Duration(seconds: 16));
//      stdin.readLineSync();

    });

    test("localSearch", () async {
//      AUTH.initLocal();
      AUTH.initRemote();

      var searchQuery = 'klika3';
//      var searchResults = await rs.RsGxsForum.localSearch(searchQuery);
      var searchResults = await repo.sortedLocalSearch(searchQuery);
      print(searchResults);

    });

    test("isBookmarked", () async {
      AUTH.initLocal();
//      AUTH.initRemote();
      var forumId = "15c6657b88d3e5a935d30774de7a2723" ;
      var postId = '5f2c8ee7692a27531c73f88a762ed3256372b9a9';
      var searchResults = await repo.isBookmarked(forumId, postId);
      assert(searchResults,true);

      forumId = "fkmkam" ;
      postId = 'aasdad';
      searchResults = await repo.isBookmarked(forumId, postId);
      assert(searchResults,false);

      print(searchResults);

    });

  });

  group('RsGxsCircles', () {
    test("createCircle", () async {
      AUTH.initLocal();
      // AUTH.initRemote();

      // var res = await rs.RsGxsCircles.createCircle(circleName, RsGxsCircleType.PUBLIC);
      var res = await repo.createCircle("circleName");
      print(res);
      // 8bfd87cabebd7e198f2d4cf4a3bd9aa3
    });

    test("getCircleDetails", () async {
      AUTH.initLocal();
      // AUTH.initRemote();
      var list = await rs.RsGxsCircles.getCirclesSummaries();
      for (var circle in list){
        print("Found circle:");
        print(circle);
      }
      // String id = "8bfd87cabebd7e198f2d4cf4a3bd9aa3";
      // var detail = await rs.RsGxsCircles.getCircleDetails(id);
      // print(detail);
    });

    test("createCircledContent", () async {
      AUTH.initLocal();
      // AUTH.initRemote();

      // var forum = await repo.findOrCreateRepoForum(
      //     circleId: "8bfd87cabebd7e198f2d4cf4a3bd9aa3"
      // );

      models.PostData postData = new models.PostData();
      postData.metadata.title = "testing circles2";
      postData.mBody.text = "#circles #circles2";
      postData.filename = "";
      postData.metadata.circle = "8bfd87cabebd7e198f2d4cf4a3bd9aa3";

      var res = await repo.publishPost(postData);
      print(res);

    });

    test("getSortedCircles", () async {
      // AUTH.initLocal();
      AUTH.initRemote();
      var list = await repo.getSortedCircles();
      print(list.length);
      print(list.toString());
    });

    test("editCircle", () async {
      AUTH.initLocal();
      // AUTH.initRemote();
      String circleId = "8bfd87cabebd7e198f2d4cf4a3bd9aa3";
      var mInvitedMembers = [
      "1d707bc52754bb2953221c3541e760aa",
      "4b01ba1ab1ac85ef10cef012ea8b937e",
      "6d0abe5ae6b7113c93530b50c6a10c0b",
      "bdbd397cf7800c5e085968e185633b50"
      ];

      var list = await repo.editCircle(circleId, "Olimpiadas", mInvitedMembers);
    });

    test("testRemoteSupport", () async {

      print("Result");
      print(await repo.supportRemoteControl("127.0.0.1"));

    });
  });
}
