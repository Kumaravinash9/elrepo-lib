/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'dart:async';
import 'dart:convert';
import 'package:elrepo_lib/src/rsModels.dart';

import 'constants.dart' as cnst;
import 'retroshare.dart' as rs;
import 'models.dart' as models;
import 'package:mime/mime.dart' as mime;
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;

List forumTypes = ["CONTENT", "TAG", "USER", "BOOKMARKS"];

class PostRoles {
  static const String post = "post";
  static const String comment = "comment";
}

Future<String> getAccountStatus() async {
  final isLoggedIn = await rs.RsLoginHelper.isLoggedIn();
  if (isLoggedIn) {
    print("Is logged in");
    return cnst.AccountStatus.isLoggedIn;
  } else {
    if (await rs.RsLoginHelper.hasLocation()) {
      print("Has Location -> Login");
      return cnst.AccountStatus.hasLocation;
    } else {
      print("Does Not Have Location -> SignUp");
      return cnst.AccountStatus.hasNoLocation;
    }
  }
}

Future<dynamic> subscribeToRepoForums() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final elRepoForums = allForums
      .where((i) => i["mGroupName"] == "elRepo_${cnst.API_VERSION}_")
      .toList();
  print("Found elRepo.io forums $elRepoForums");
  Map forum;
  for (forum in elRepoForums) {
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
  }
}

Future<int> login(String password, [Map location]) async {
  if (location == null) {
    location = await rs.RsLoginHelper.getDefaultLocation();
    print('logging in $location');
  }
  final responseStatus = await rs.RsLoginHelper.login(location, password);
  return responseStatus;
}

Future<Map> signUp(String password, String locationName, {String api_user}) async {
  print('creating account for $locationName');
  final location =
    await rs.RsLoginHelper.createLocation(locationName, password, api_user: api_user);
  print("Location: $location");
  if(api_user != null) {
    // Set the API user to start to work with. This is used on the gateway where
    // the default api user is not elrepo.io
    rs.authApiUser = api_user;
  }
  return location;
}

/// Separate funcion to add the list of tiers. Better here to use it in the gateway also
void befriendingTiers() async {
  await rs.befriendTier1(hostname: "margaretsanger");
  await rs.befriendTier1(hostname: "robertocarlos");
}

Future<bool> prepareSystem(downloadDir, partialsDir) async {
  // Is a good idea to exchange RS invites each time to update the public IP of the nodes
  befriendingTiers();
  print(
      "Setting Retroshare directories. \n Download and shared: $downloadDir\n Partials: $partialsDir");
  rs.RsFiles.setDownloadDirectory(downloadDir);
  rs.RsFiles.addSharedRepoDirectory(downloadDir);
  rs.RsFiles.setPartialsDirectory(partialsDir);

  await subscribeToRepoForums();
  rs.RsGxsForum.requestSynchronization();
  return true;
}

String getForumName(String forumType, [String label = ""]) {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  return "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
}

Future<String> findOrCreateRepoForum(
    {String forumType = "CONTENT", String label = "", String circleId = ''}
    ) async {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  String forumName =
      "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
  print("Find or create $forumName");
  var result = await rs.RsGxsForum.getForumsSummaries();
  var elRepoForums = result.where(
          (i) =>
          ((i["mGroupName"] == forumName
                  && circleId.isEmpty
                  && i['mCircleId'] == '00000000000000000000000000000000') ||
              (i["mGroupName"] == forumName && (circleId.isNotEmpty && i["mCircleId"] == circleId) ))
  ).toList();
  var forumId;
  if (elRepoForums.length > 0) {
    print("Found existing elRepo.io forums");
    forumId = elRepoForums[0]["mGroupId"];
  }
  else {
    forumId = await rs.RsGxsForum.createForumV2(forumName, circleId: circleId);
    print("No elRepo.io forums found, creating: $forumName");
    rs.RsGxsForum.subscribeToForum(forumId, true);
  }
  return forumId;
}

String summaryFromBody(bodyText) {
  String summary;
  if (bodyText.length > cnst.SUMMARY_LENGTH) {
    summary = bodyText.substring(0, cnst.SUMMARY_LENGTH);
    summary = "$summary...";
  } else {
    summary = bodyText;
  }
  return summary;
}

/// Publish post to RetroShare node
///
/// It search for #tags inside [models.PostData.mBody.text] if [parseTags] is not
/// defined.
///
/// It hash the  [models.PostData.filePath] and add it to RetroShare
///
/// Also creates tag, content, and user forums.
Future<models.PublishData> publishPost(models.PostData postData,
    {bool parseTags = true}) async {
  List fileHashAndLink = [];
  if (postData.filePath != null && postData.filePath != "") {
    final hashingFile = await rs.RsFiles.extraFileHash(postData.filePath);

    if (hashingFile == true) {
      print("Hashing file ${postData.filePath}");
      fileHashAndLink = await rs.waitForFileHashAndLink(postData.filePath);
      postData.mBody.payloadLinks.add(models.PayloadLink(
          postData.filename, fileHashAndLink[0], fileHashAndLink[1]));
      print("Hashing done for ${postData.filePath}. Result: $fileHashAndLink");
      postData.metadata.contentType = getContentTypeFromPath(postData.filePath);
    }
  }

  if (postData.metadata.summary == null) {
    postData.metadata.summary = summaryFromBody(postData.mBody.text);
  }

  if (parseTags) postData.mBody.tags += findHashTags(postData.mBody.text); // Find hashtags inside publication text

  final forumId = await findOrCreateRepoForum( circleId: postData.metadata.circle ?? '');
  final msgId = await rs.RsGxsForum.createPost(
      forumId,
      jsonEncode(postData.metadata),
      jsonEncode(postData.mBody),
      rs.authIdentityId);

  final linkPostMetadata = jsonEncode(
      models.PostMetadata.generateLinkPostMetadata(
          postData.metadata, msgId, forumId));

  for (String tag in postData.mBody.tags) {
// we add the reference to the real post
    final tagForumId = await findOrCreateRepoForum(forumType: "TAG", label: tag, circleId: postData.metadata.circle ?? '');
    print("Creating post in $tagForumId");
// we don't save the message body in a linkPost
    await rs.RsGxsForum.createPost(
        tagForumId, linkPostMetadata, "", rs.authIdentityId);
  }
// we also create a linkPost in the user forum to keep track of content published.
  final userForumId = await findOrCreateRepoForum(forumType: "USER", label: rs.authIdentityId);
  // This await is needed because if you are testing sometimes the program finishes before create userforum post
  await rs.RsGxsForum.createPost(
      userForumId, linkPostMetadata, "", rs.authIdentityId);

  models.PublishData publishData = new models.PublishData(forumId, msgId, postData.mBody.tags);

  return publishData;
}

/// Add a publication to bookmarks forum
///
/// [referredForumId] is the CONTENT forum id and the [referredPostId] is the
/// post inside the CONTENT forum that will link.
Future<void> bookmarkPost(String referredForumId, String referredPostId, models.PostMetadata postMetadata) async{
  final bookmarksForumId = await findOrCreateRepoForum(
      forumType: "BOOKMARKS",  label: rs.authIdentityId);
  print("Checking for bookmark in forum: $bookmarksForumId");
  final referred = "$referredForumId $referredPostId";
  final bookmarkHeaders =
      await getPostHeaders("BOOKMARKS", rs.authIdentityId);
// TODO(nicoechaniz): find a more efficient way to do this
  List duplicates = bookmarkHeaders
      .where((i) => jsonDecode(i["mMsgName"])["referred"] == referred)
      .toList();
  if (duplicates.length == 0) {
    final linkPostMetadata =
    models.PostMetadata.generateLinkPostMetadata(
        postMetadata, referredPostId, referredForumId);
    await rs.RsGxsForum.createPost(
        bookmarksForumId,
        json.encode(linkPostMetadata.toJson()),
        "",
        rs.authIdentityId);
  }
}

Future<List> getTagNames() async {
  print("Getting tag names");
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final tagForums = allForums
      .where((i) => i["mGroupName"]
      .toString()
      .startsWith("${cnst.FORUM_PREPEND}${cnst.API_VERSION}_TAG_"))
      .toList();
  print("Tag forums $tagForums");

  Map forum;
  var allTags = new Set();
  String tagName;
  String groupName;
  for (forum in tagForums) {
    // TODO(): check to only subscribe when needed.
    if (forum["mSubscribeFlags"] == 8)
      await rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    groupName = forum["mGroupName"].toString();
    tagName =
        groupName.substring(groupName.lastIndexOf("_TAG_") + '_TAG_'.length);
    allTags.add(tagName);
  }
  print("Found tags: $allTags");
  final tagsList = allTags.toList();
  print("tags list is $tagsList");
  return (tagsList);
}

void syncForums() {
  // TODO(nicoechaniz): this is not currently working as expected from the RetroShare end.
  // re-visit once the functionality is improved in RetroShare.
  rs.RsGxsForum.requestSynchronization();
}

/// Return a list of followed sslIds
Future<List<String>> getFollowedAuthors() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  print("All forums: $allForums");
  final followingForums = allForums.where(
          (i) => i["mGroupName"]
          .toString()
          .startsWith("${cnst.FORUM_PREPEND}${cnst.API_VERSION}_USER_")
          && i["mSubscribeFlags"] == 4
  ).toList();
  print("Followed User forums: $followingForums");
  return [
    for (Map forum in followingForums)
      forum["mGroupName"].toString()
          .substring(forum["mGroupName"].toString().lastIndexOf("_USER_") + '_USER_'.length)
  ];
}

Future<List> getFollowedAuthorsDetails() async {
  print("Getting authors data");

  var authorsDetails = [
    for (var authorId in await getFollowedAuthors())
      await rs.RsIdentity.getIdDetails(authorId)];

  print("Author details found: $authorsDetails");
  return authorsDetails;
}

Future<String> getAuthorName(String authorId) async {
  final identityDetails = await rs.RsIdentity.getIdDetails(authorId);
  return identityDetails["mNickname"];
}

Future<String> createComment(String forumId, String metadata,
    String commentBody, String parentPostId) async {
  final msgId = await rs.RsGxsForum.createPost(
      forumId, metadata, commentBody, rs.authIdentityId, parentPostId);
  return msgId;
}


Future<Map> getUserForum(String identityId) async {
  final userForumName = getForumName("USER", identityId);
  final requestedForums = await getForumsByName(userForumName);
  // there should be only one forum for each user, so we pick the first result
  final userForum = requestedForums[0];
  return userForum;
}

Future<List> getForumMetadata(String forumId) {
  return rs.RsGxsForum.getForumMsgMetaData(forumId);
}

void subscribeToForum(Map forum){
//void followUser(String identityId){
  rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
  rs.RsGxsForum.requestSynchronization();
}

Future<Map> getIdDetails (String identityId) {
  final identityDetails = rs.RsIdentity.getIdDetails(identityId);
  return identityDetails;
}

Future<List> getForumsByName(String forumName) async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final allForumNames = allForums.map((i) => i["mGroupName"]).toList();
  print("All known forum names: $allForumNames");
  final requestedForums =
  allForums.where((i) => i["mGroupName"] == forumName).toList();
  return requestedForums;
}

Future<List> getPostHeaders(
    [String forumType = "CONTENT", String label = ""]) async {
  final requestedForumName = getForumName(forumType, label);
  print("Retrieving post headers from " + requestedForumName);
  Map forum;
  List allPosts = [];
  final requestedForums = await getForumsByName(requestedForumName);
  for (forum in requestedForums) {
    print('Found forum: ${forum["mGroupName"]} / id: ${forum["mGroupId"]}');
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values before trying to subscribe
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    allPosts.addAll(await _getPostsMetadata(forum));
  }
  return allPosts;
}

Future<List> getPostDetails(String forumId, String postId,
    [bool isLinkPost]) async {
  final details = await rs.RsGxsForum.getForumContent(forumId, [postId]);
  if (isLinkPost) {
    var postMetadata =
      models.PostMetadata.fromJsonString(details[0]['mMeta']['mMsgName']);
    try {
      final realPostIds = postMetadata.referred.split(' ');
      final realPostDetails =
        await rs.RsGxsForum.getForumContent(realPostIds[0], [realPostIds[1]]);
      return realPostDetails;
    } catch (e) {
      print('Could not retrieve original post $e');
      return [];
    }
  }
  return details;
}

/// From a list of post headers, get all referred links and return its in a map
/// forumId : [List of requested posts]
Map<String,List<String>> getReferredsFromPostHeader(List postHeaders) {
// To optimize calls to the API, lets group all requested posts in a map where:
// forumId : [List of requested posts]
  var forumsToPosts = <String, List<String>>{};
  postHeaders.forEach((postMeta) {
    try {
      List referred = json.decode(postMeta["mMsgName"])["referred"].split(" ");
      if (!forumsToPosts.containsKey(referred[0]))
        forumsToPosts[referred[0]] = [];
      forumsToPosts[referred[0]].add(referred[1].toString());
    } catch (e) {
      print(e);
    }
  });
  return forumsToPosts;
}

/// Return a list of real posts from a list of postHeaders that contain referred
/// content.
/// Example: you want to retrieve post headers from CONTENT forums using
/// BOOKMARKS post headers.
Future<List> getContentPostsFromReferreds(List postHeaders) async {
  print("Get post headers from referred contents list");
  var forumsToPosts = getReferredsFromPostHeader(postHeaders);
  var msgMeta = <dynamic>[];
  for(var forumId in forumsToPosts.keys) {
    var temp = await rs.RsGxsForum.getForumMsgMetaData(forumId);
    temp.removeWhere((post) => !forumsToPosts[forumId].contains(post["mMsgId"]));
    msgMeta.addAll(temp);
  }
  return msgMeta;
}

/// From a post metadata object return referred content real post id
getRealPostId(postMetadata) {
  try {
    final realPostId = postMetadata.referred.split(" ")[1];
    return realPostId;
  } catch (error) {
    print("Could not retrieve original post: $error");
    return null;
  }
}

String _getUserForumName() =>
    "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_USER_${rs.authIdentityId}";

String _getUserBookmarkName() =>
    "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_BOOKMARKS_${rs.authIdentityId}";

// TODO(nicoechaniz): the cache implementation is very naive, it's there just as an experiment.
//  We need to implement this with sqlite storage or something similar
List bookmarksCache;
List selfPostsCache;
Duration cacheDuration = Duration(seconds: 30);
DateTime bookmarksLastRefresh;
DateTime selfPostsLastRefresh;

Future<List<dynamic>> getBookmarkedIds() async {
  if (bookmarksLastRefresh == null) {
    bookmarksLastRefresh = DateTime.now();
  }
  Duration timeDelta = DateTime.now().difference(bookmarksLastRefresh);
  if (timeDelta < cacheDuration && bookmarksCache != null) {
    print("------------------ return bookmarks from cache");
    return bookmarksCache;
  }
  List bookmarkedIds;
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final bookmarkForumName =
  _getUserBookmarkName();
  final bookmarksForum =
  allForums.where((i) => i["mGroupName"] == bookmarkForumName).toList();
  if (bookmarksForum.length > 0) {
    final bookmarkedMessages =
    await rs.RsGxsForum.getForumMsgMetaData(bookmarksForum[0]["mGroupId"]);
    bookmarkedIds = bookmarkedMessages
        .map((item) =>
        getRealPostId(models.PostMetadata.fromJsonString(item["mMsgName"])))
        .toList();
  }
  bookmarksCache = bookmarkedIds;
  bookmarksLastRefresh = DateTime.now();
  return bookmarkedIds;
}

Future<List<dynamic>> getSelfPostIds() async {
  if (selfPostsLastRefresh == null) {
    selfPostsLastRefresh = DateTime.now();
  }
  Duration timeDelta = DateTime.now().difference(selfPostsLastRefresh);
  if (timeDelta < cacheDuration && selfPostsCache != null) {
    return selfPostsCache;
  }
  List selfPostsIds;
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  print("Fetching User posts");
  final userForumName =
  _getUserForumName();
  final userForum =
  allForums.where((i) => i["mGroupName"] == userForumName).toList();
  if (userForum.length > 0) {
    final userMessages =
    await rs.RsGxsForum.getForumMsgMetaData(userForum[0]["mGroupId"]);
    selfPostsIds = userMessages
        .map((item) =>
        getRealPostId(models.PostMetadata.fromJsonString(item["mMsgName"])))
        .toList();
  }
  selfPostsCache = selfPostsIds;
  selfPostsLastRefresh = DateTime.now();
  return selfPostsIds;
}

Future<String> getPathIfExists(hash) async {
  final fileInfo = await rs.RsFiles.alreadyHaveFile(hash);
  if (fileInfo is Map) {
    return fileInfo["path"];
  } else
    return null;
}

String getContentTypeFromPath(String filePath) {
  final fileName = path.basename(filePath);
  final fileType = mime.lookupMimeType(fileName);
  var fileRootType = "";
  if (fileType is String) {
    fileRootType = fileType.split("/")[0];
  }
  return fileRootType;
}

List<String> findHashTags(String lookupString) {
  RegExp exp = new RegExp(r"(#(?:[^\x00-\x7F]|\w)+)");
  Iterable<RegExpMatch> matches = exp.allMatches(lookupString);
  List<String> hasTagsList = [];
  if (matches.isNotEmpty) {
    hasTagsList = matches.map((x) => x[0].substring(1)).toList();
  }
  print("Found tags: $hasTagsList");
  return hasTagsList;
}

/// Get a list of downloading files details
Future<List<dynamic>> getDownloadingListDetails( ) async {
  var hashes = await rs.RsFiles.fileDownloads();
  return [for(String hash in hashes) await rs.RsFiles.fileDetails(hash)];
}

/// Get original CONTENT "post metadata" from BOOKMARKS "post metadata". If
/// [downloading] is true return just downloading bookmarks, else return already
/// downloaded bookmarks.
Future<List<dynamic>> getBookmarks({bool downloading=false}) async {
  var bookmarkHeaders = await getPostHeaders("BOOKMARKS", rs.authIdentityId);
  var referredLinks =
    await getReferredsFromPostHeader(
      await getPostHeaders("BOOKMARKS", rs.authIdentityId)
    );
  var hashes = await rs.RsFiles.fileDownloads();
  var returnList = <dynamic>[ ];
  for (var forumId in referredLinks.keys) {
    var x = await rs.RsGxsForum.getForumContent(forumId, referredLinks[forumId]);
    returnList.addAll(x);
  }
  returnList.removeWhere((msg) {
    // Delete malformed if body is malformed. Can happen due to a bad referred content
    if (msg['mMsg'] == "") return true;
    var postbody = models.PostBody.fromJson(msg['mMsg']);
    // If payloadlinks are not empty mean that can be downloading content
    if (postbody.payloadLinks.isNotEmpty) {
      if (downloading) {
        for (var payloadLink in postbody.payloadLinks)
          // If you are looking for downloading content and hashes contains one
          // payloadlink don't remove it
          if (hashes.contains(payloadLink.hash)) return false;
      } else {
        for (var payloadLink in postbody.payloadLinks)
          // If you are looking for already download content and hashes contains
          // one  payloadlink remove it
          if (hashes.contains(payloadLink.hash)) return true;
      }
    }
    // If payload link is empty mean that is already downloaded, so remove it if
    // you are looking downloading content
    return downloading;
  });
  return returnList.map((x) => x = x["mMeta"]).toList();
}

/// Return a future list of sslids details.
///
/// The details object is modified and added a boolean isConnected value
Future<List<dynamic>> getSslPeerList( ) async {
  var onlineList = await rs.RsPeers.getOnlineList();
  var result =  [];
  if (onlineList.isNotEmpty) {
    result.addAll([
      for (var sslId in onlineList)
        await rs.RsPeers.getPeerDetails(sslId)..["isConnected"] = true
    ]);
  }
  var offlineList = await rs.RsPeers.getFriendList();
  if (offlineList.isNotEmpty) {
    offlineList.removeWhere((sslId) => onlineList.contains(sslId));
    result.addAll([
      for (var sslId in offlineList) await rs.RsPeers.getPeerDetails(sslId)
        ..["isConnected"] = false
    ]);
  }
  return result;
}

/// Execute rs.RsBroadcastDiscovery.getDiscoveredPeers() and add all returned
/// peers as friends.
Future<List> doPromiscuity() async {
  final peers = await rs.RsBroadcastDiscovery.getDiscoveredPeers();
  var toAdd = [];
  if (peers.length > 0) {
    for (var peer in peers) {
      if(!await rs.RsPeers.isFriend(peer['mSslId']))
        toAdd.add(peer);
    }
    print("Peers found: $peers");
    rs.addFriends(toAdd);
  }
  return toAdd;
}

/// This function will return a digested CONTENT forums posts headers metadata.
///
/// It mean that this function sort the content to show by certain priorities,
/// for example, it show following users first.
Future<List<dynamic>> exploreContent() async {
  var contentsList = await getPostHeaders();
  contentsList.removeWhere((element) => element["mAuthorId"] == rs.authIdentityId);
  return await sortPostList(contentsList);
}

/// Sort a post headers list
///
/// It return at the beggining followed authors
Future<List<dynamic>> sortPostList(List<dynamic> contentsList) async {
  var followedAuthors = await getFollowedAuthors();
  for (var post in contentsList) {
    if(followedAuthors.contains(post["mAuthorId"])) {
      contentsList.remove(post);
      contentsList.insert(0, post);
    }
  }
  return contentsList;
}

/// Distant forum search. Ask to online nodes for a String.
///
/// Wrapper that return a Stream Subscription. Where [callback] argument is a
/// function that receive the events with the same search code. IMPORTANT: it
/// return only CONTENT forums
Future<StreamSubscription> distantSearch(String text, Function(dynamic) callback) async {
  var searchId = await rs.RsGxsForum.distantSearchRequest(text);
  return await rs.RsEvents.registerEventsHandler(
      RsEventType.GXS_FORUMS,
          (Map<String, dynamic> event) {
        if (event['mForumEventCode'] == RsForumEventCode.DISTANT_SEARCH_RESULT.index
            && event['mSearchId'] == searchId){
          print("Found search event!");
          print(event);

          if (callback != null) {
            callback([
              // Delete non CONTENT forums
              for(var post in event['mSearchResults'])
                if (post["mGroupName"] == getForumName("CONTENT")) post
          ]);
          }
        }
      }
  );
}

/// Local search wrapper that return sorted list of local searched posts
///
/// Order the post by following and return only content posts
Future<List<dynamic>> sortedLocalSearch(String text) async {
  return await sortPostList( [for (var post in await rs.RsGxsForum.localSearch(text))
    if (post["mGroupName"] == getForumName("CONTENT")) post
//    if (post["mGroupName"] == "elRepo.io_0.0.13_CONTENT") post
  ]);
}


/// Delete from a list of posts headers all the ocurrences on other.
///
/// Used to search ocurrences from a list of post headers into another and
/// remove it. Where [original] is the main list, where to search the ocurrences,
/// and [newList] is the list that we return and where we are going to delete
/// the repeated ocurrences.
List<dynamic> deleteFromPostList(
    List<dynamic> original, List<dynamic> newList) {
  newList.removeWhere((newElement) {
    for(var post in original) {
      if(post['mMsgId'] == newElement['mMsgId']) return true;
    }
    return false;
  });
  return newList;
}

/// Check if a post exists on the user bookmarks
///
/// Used, for example, to know if a publication with a payload is already
/// bookmarked
Future<bool> isBookmarked(forumId, postId) async {
  for (var post in await getPostHeaders("BOOKMARKS", rs.authIdentityId)) {
    if(jsonDecode(post['mMsgName'])["referred"] == "$forumId $postId" ) return true;
  }
  return false;
}


// ********** CIRCLEs ********** //

/// Create a circle and add your default identity to it
///
/// Where [identity] is your own identity to add to the circle. You can also
/// define a list of [invitedIds] to send invites to the circle.
/// Return circleId
Future<String> createCircle(String circleName, [String identity, List<String> invitedIds]) async {
  print('Creating circle $circleName');
  String circleId = await rs.RsGxsCircles.createCircle(circleName, RsGxsCircleType.PUBLIC);
  identity ??= rs.authIdentityId;
  // First add yourself to the circle
  if (identity != null) {
    await rs.RsGxsCircles.requestCircleMembership(circleId, identity);
    await rs.RsGxsCircles.inviteIdsToCircle([identity], circleId);
  }
  // Invite people
  if (invitedIds != null) {
    await rs.RsGxsCircles.inviteIdsToCircle(invitedIds, circleId);
  }
  return circleId;
}

/// Return a list of subscribed circles
///
/// This circles have the `mSubscribeFlag` set to `7`
Future<List<dynamic>> getSubscribedCircles() async {
  var circleList = await rs.RsGxsCircles.getCirclesSummaries();
  return circleList..removeWhere((circle) => circle['mSubscribeFlags'] != 7);;
}

/// Edit a circle
///
///
Future<Map<String, dynamic>> editCircle(String circleId, String circleNewName, List<String> invitedPeople) async {
  var circleList = await rs.RsGxsCircles.getCirclesInfo([circleId]);
  circleList[0]["mMeta"]["mGroupName"] = circleNewName;
  circleList[0]["mInvitedMembers"] = invitedPeople;
  return await rs.RsGxsCircles.editCircle(circleList[0]);
}

/// Return a list of circlesSummaries + circleDetails
///
/// First call circle summaries and then append on the response a `["details"]`
/// object with the call to getCircleDetails. Should be called twice because
/// Retroshare backend needs to cache circle details
Future<List<dynamic>> getCirclesDetails() async{
  var circleList = await rs.RsGxsCircles.getCirclesSummaries();
  // Get details from the circle
  for(var circle in circleList) {
    try {
      circle["details"] = await rs.RsGxsCircles.getCircleDetails(circle['mGroupId']);
    } catch(e) {
      print(e);
    }
  }
  return circleList;
}

/// Get sorted list of circles.
///
/// It get a list of circle details and sort it by `mSubscriptionFlags`. In this
/// version **the circles you not belong are deleted**. It also **autosubscribe** to a
/// circle if you recieved an invitation from the admin.
Future<List<dynamic>> getSortedCircles() async {
  var circleList = await getCirclesDetails();
  // Delete circles you don't own, you don't belong or you are not invited
  circleList.removeWhere((circle) {
    var mSubscriptionFlag = <String, dynamic>{};
    // If details can't be retrieved
    if (!circle.containsKey("details")) return true;
    // Check if your identity is present on the circle
    for(var id in circle["details"]['mSubscriptionFlags']) {
      if (id['key'] == rs.authIdentityId) mSubscriptionFlag = id;
    }
    if(mSubscriptionFlag.isEmpty) return true;
    // Check if the user tried to request membership. THIS IS NOT SUPPORTED IN THIS VERSION
    // We only support admin invites.
    else if(
      mSubscriptionFlag['value'] & RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST !=
          RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST ) return true;
    // Store your identity flag in a variable to don't search it later
    circle["details"]['myFlag'] = mSubscriptionFlag;
    return false;
  });
  // Here our circList have only circles you own, you belong and you are invited. 
  for(var circle in circleList){
    // Put owned circles at the beggining
    if(circle["details"]["mAmIAdmin"]) {
      circleList.remove(circle);
      circleList.insert(0, circle);
    }
    // If is invited but invite is not accepted, accept it.
    else if (circle["details"]['myFlag']['value'] != 7 &&
        // Probably this bit wise operation could be replaced by ['details']['mAmIAllowed']
        circle["details"]['myFlag']['value'] &
          RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST ==
            RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST) {
      await rs.RsGxsCircles.requestCircleMembership(circle['mGroupId']);
    }
  }
  return circleList;
}

// ********** elrepo.io gateway related functions ********** //

models.GatewayAuthObject originalAuth;

/// Function that enables comunication to a remote RS
Future<bool> changeRsPrefix (String newRsPrefix) async {
  rs.setRetroshareServicePrefix(newRsPrefix);
  if (!await rs.isRetroshareRunning()) {
    rs.setRetroshareServicePrefix();
    print ("Can't connect to remote RS");
    return false;
  }
  print("The new RS prefix is: " + rs.getRetroshareServicePrefix());
  return true;
}

/// Call to an aqueduct elrepo.io instance to get an authorization token.
///
/// This is used on the remote control workflow for elrepo.io. Where
/// [newRsPrefix] should be something like: 'http://aqueductIp:8888/rsremote/'
/// The received user/pass for the specified [apiUser] will be used as new auth token
Future<bool>  requestGatewayAuthorization (String newRsPrefix, String apiUser) async {
  try {
    // Here the POST api call to aqueductIp:8888/rsremote/:newUser
    final res = await http.post(newRsPrefix+'/'+apiUser);
    // The response is a username and a new random password associated
    if (res != null && res.statusCode == 200){
      var decoded = models.GatewayAuthObject.fromJson(jsonDecode(res.body));

      // If original auth == null store it. This should prevent to don't lose the
      // original auth if requestGatewayAuthorization() executed twice or more
      originalAuth ??= models.GatewayAuthObject(rs.authApiUser, rs.authPassphrase, rs.authIdentityId);

      rs.initRetroshare(
          apiUser: decoded.user,
          passphrase: decoded.password,
          identityId: decoded.identity
      );
      return true;
    }
  } catch (err) {
    print('Cant get new auth token from elrepo.io gateway');
    return false;
  }
}

/// This funcion change on meory RsPrefix by [newRsPrefix] and request a new [apiUser]
/// token auth to talk, tipically, with an aqueduct elrepo.io gateway
Future<bool> enableRemoteControl(String newRsPrefix, String apiUser) async {
  var enableSuccess = await changeRsPrefix(newRsPrefix) &&
      await requestGatewayAuthorization(newRsPrefix, apiUser);
  if (!enableSuccess) disableRemoteControl();
  return enableSuccess;
}

/// Set the default values after enableRemoteControl() is executed
Future<void> disableRemoteControl( ) async {
  rs.setRetroshareServicePrefix();
  if (originalAuth != null) {
    rs.initRetroshare(
        apiUser: originalAuth.user,
        passphrase: originalAuth.password,
        identityId: originalAuth.identity
    );
    originalAuth = null;
  }
}

/// Check if an [ip] has the "rsremote" endpoint.
///
/// This should mean that is an aqueduct gateway node that accepts remote control.
Future<bool> supportRemoteControl(String ip) async {
  String remote = "http://${ip}:8888/rsremote";
  print("Check remote control for $remote");
  try {
    final response = await http.get(remote);
    return (response == null || response?.statusCode != 200) ? false:
      jsonDecode(utf8.decode(response.bodyBytes))["retval"];
  } catch (e){
    print(e);
    return false;
  }
}

/// Get user sync contect based on BOOKMARKS and USER forum
Future<List> getSyncedContentMetadata() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final userForum = allForums.where((i) =>
  i["mGroupName"] == _getUserForumName() ||
      i["mGroupName"] == _getUserBookmarkName()).toList();
  var userForumMetadatas = await _getForumListMetadata(userForum);
// Get refered content
  Map<String, List<String>> referredContent = Map();
  userForumMetadatas.forEach((element) {
    List<String> referred = jsonDecode(element['mMsgName'])["referred"].split(
        " ");
    referredContent[referred[0]] =
    referredContent[referred[0]]?.isEmpty ?? true
        ? [...[referred[1]]]
        : [...referredContent[referred[0]], ...[referred[1]]
    ];
  });
  var resultingList = await
  Future.wait(
// This return a list of lists, so we have to expand it to get a single list of posts objects
      referredContent.entries.map(
              (e) async => rs.RsGxsForum.getForumContent(e.key, e.value)));
  return resultingList.expand((i) => i).toList();
}

/// From a [forumList], return the list with all the posts metadata associated
Future<List<dynamic>> _getForumListMetadata(List<dynamic> forumList) async {
  List<dynamic> allPosts = [];
  for (Map forum in forumList) {
    allPosts.addAll(await _getPostsMetadata(forum));
  }
  return allPosts;
}

/// From a [forum] object, return the metadata associated in a Post iterable
Future<Iterable> _getPostsMetadata(Map forum) async {
  var posts = await rs.RsGxsForum.getForumMsgMetaData(forum["mGroupId"]);
  return posts.where((post) {
// Try to parse PostMetadata.fromJsonString to prevent malformed json
    try {
      models.PostMetadata postMetadata =
       models.PostMetadata.fromJsonString(post["mMsgName"]);
      return postMetadata.role == PostRoles.post;
    } on Exception {
      return false;
    }
  });
}

Future<String> getLocationPath() async {
  String locationPath = "";
  final sharedDirs = await rs.RsFiles.getSharedDirectories();
  if (sharedDirs.length > 0) {
    final downloadPath = sharedDirs[0]["filename"];
    final pathComponents = path.split(downloadPath);
    pathComponents.removeLast();
    locationPath = path.joinAll(pathComponents);
  }
  return locationPath;
}

Future<Map> getLocation() async{
  final location = await rs.RsLoginHelper.getDefaultLocation();
  return location;
}
