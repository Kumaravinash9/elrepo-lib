/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'dart:convert';
import 'repo.dart' as repo;

// options

class ContentTypes {
  static const String ref = "ref";
  static const String image = "image";
  static const String audio = "audio";
  static const String video = "video";
  static const String document = "document";
  static const String text = "text";
  static const String file = "file";
}

class MarkupTypes {
  static const String plain = "plain";
  static const String markdown = "markdown";
  static const String html = "html";
}


// models

class PayloadLink {
  String filename = '';
  String hash = '';
  String link = '';
  // TODO(nicoechaniz): hash can be extracted from the link. Implement that where needed and remove hash from here
  Map<String, dynamic> toJson() =>
      {'filename': filename, 'hash': hash, 'link': link};

  PayloadLink(this.filename, this.hash, this.link);
}

class PostBody {
  String text = '';
  List<PayloadLink> payloadLinks = [];
  List<String> tags = [];

  PostBody();

  Map<String, dynamic> toJson() => {
    'text': text,
    'payloadLinks': payloadLinks,
    'tags': tags
  };

  PostBody.fromJson (String rawBody){
    Map jsonBody = jsonDecode(rawBody);
    this.text = jsonBody["text"];
    if (jsonBody["payloadLinks"].length > 0) {
      for (Map linkData in jsonBody["payloadLinks"]) {
        this.payloadLinks.add(PayloadLink(
            linkData["filename"], linkData["hash"], linkData["link"]));
      }
    }
    if (jsonBody["tags"] != null && jsonBody["tags"].length > 0) {
      this.tags = jsonBody["tags"].cast<String>();
    }
  }
}

class PostMetadata {
  String title;
  String summary;
  String markup;
  String contentType;
  String role;
  String referred;
  String circle;

  PostMetadata(
      {this.title,
      this.summary,
      this.markup,
      this.contentType,
      this.referred,
      this.circle,
      this.role});

  // get metadata for a linkPost
  PostMetadata.generateLinkPostMetadata(
      PostMetadata postMetadata, String msgId, String forumId)
      : this(
            title: postMetadata.title,
            summary: postMetadata.summary,
            markup: postMetadata.markup,
            contentType: postMetadata.contentType,
            role: postMetadata.role,
            circle: postMetadata.circle,
            referred:
                "$forumId $msgId"); // reference to the actual content post

  Map<String, dynamic> toJson() => {
        'title': title,
        'summary': summary,
        'markup': markup,
        'contentType': contentType,
        'role': role,
        'referred': referred,
        'circle': circle
      };

  factory PostMetadata.fromJsonString(String jsonString) {
    try {
      final json = jsonDecode(jsonString);
      return PostMetadata(
          title: json['title'],
          summary: json['summary'],
          markup: json['markup'],
          contentType: json['contentType'],
          role: json['role'],
          circle: json['circle'],
          referred: json['referred']);
    } catch (e) {
      print("Can't decode " + jsonString);
      return PostMetadata(
          title: "bad parsing",
          summary: "bad parsing",
          markup: MarkupTypes.plain,
          contentType: ContentTypes.text,
          role: repo.PostRoles.post,
          referred:  "");
    }
  }
}

class PostData {
  PostMetadata metadata = new PostMetadata(
      markup: MarkupTypes.plain,
      contentType: ContentTypes.text,
      role: repo.PostRoles.post);
  PostBody mBody = new PostBody();
  String filePath = '';
  String filename = '';
  String project = ''; // Used for import datasets. The project name of the original data, ex: gutemberg
  String originUrl = ''; // Used for import datasets. Url for the original data.
}

class PublishData {
  String msgId;
  String forumId;
  List hashTags;

  PublishData(this.msgId, this.forumId, this.hashTags);
}

// ********** elrepo.io gateway related functions ********** //

class GatewayAuthObject {

  String user, password, identity;
  GatewayAuthObject(this.user, this.password, this.identity);

  GatewayAuthObject.fromJson(Map<String, dynamic>  json) :
    user = json['user'],
    password = json['password'],
    identity = json['identity'];

  Map<String, dynamic> toJson() =>
      {
        'user': user,
        'password': password,
        'identity': identity,
      };
}
