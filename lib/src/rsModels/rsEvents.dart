/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
part of rsModels;

enum RsEventType  {
/// Used internally to detect invalid event type passed = 0,
__NONE,
/// @see RsBroadcastDiscovery = 1,
BROADCAST_DISCOVERY,
/// @see RsDiscPendingPgpReceivedEvent = 2,
GOSSIP_DISCOVERY,
/// @see AuthSSL = 3,
AUTHSSL_CONNECTION_AUTENTICATION,
/// @see pqissl  = 4,
PEER_CONNECTION,
/// @see RsGxsChanges, used also in @see RsGxsBroadcast  = 5,
GXS_CHANGES,
/// Emitted when a peer state changes, @see RsPeers  = 6,
PEER_STATE_CHANGED,
/// @see RsMailStatusEvent = 7,
MAIL_STATUS,
/// @see RsGxsCircleEvent   = 8,
GXS_CIRCLES,
/// @see RsGxsChannelEvent = 9,
GXS_CHANNELS,
/// @see RsGxsForumEvent = 10,
GXS_FORUMS,
/// @see RsGxsPostedEvent  = 11,
GXS_POSTED,
/// @see RsGxsPostedEvent   = 12,
GXS_IDENTITY,
/// @see RsFiles  = 13,
SHARED_DIRECTORIES,
/// @see RsFiles  = 14,
FILE_TRANSFER,
/// @see RsMsgs    = 15,
CHAT_MESSAGE,
/// @see rspeers.h  = 16,
NETWORK,
}
