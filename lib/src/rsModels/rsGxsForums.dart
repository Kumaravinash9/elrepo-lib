/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
part of rsModels;

enum RsForumEventCode
{
UNKNOWN                  , //= 0x00,
NEW_FORUM                , //= 0x01, /// emitted when new forum is received
UPDATED_FORUM            , //= 0x02, /// emitted when existing forum is updated
NEW_MESSAGE              , //= 0x03, /// new message reeived in a particular forum
UPDATED_MESSAGE          , //= 0x04, /// existing message has been updated in a particular forum
SUBSCRIBE_STATUS_CHANGED , //= 0x05, /// forum was subscribed or unsubscribed
READ_STATUS_CHANGED      , //= 0x06, /// msg was read or marked unread
STATISTICS_CHANGED       , //= 0x07, /// suppliers and how many messages they have changed
MODERATOR_LIST_CHANGED   , //= 0x08, /// forum moderation list has changed.
VOID, //= 0x09 this is deleted from RS code but is needed here to correct following values
SYNC_PARAMETERS_UPDATED  , //= 0x0a, /// sync and storage times have changed
PINNED_POSTS_CHANGED     , //= 0x0b, /// some posts where pinned or un-pinned
DELETED_FORUM            , //= 0x0c, /// forum was deleted by cleaning
DELETED_POST             , //= 0x0d,  /// Post deleted (usually by cleaning)

/// Distant search result received
DISTANT_SEARCH_RESULT    , //= 0x0e
}
