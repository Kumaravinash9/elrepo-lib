/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elrepo_lib/constants.dart' as constants;
import 'package:elrepo_lib/retroshare.dart' as retroshare;
import 'package:elrepo_lib/models.dart' as models;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/src/constants.dart';

class AUTH {
  static const String identityId = "bdbd397cf7800c5e085968e185633b50";
  static const String locationId = "814228577bc0c5da968c79272adcbfce";
  static const String passphrase = "test";
  static const String apiUser = "test";
}

Future<void> main() async {

  retroshare.initRetroshare(
      apiUser: AUTH.apiUser,
      passphrase: AUTH.passphrase,
      identityId: AUTH.identityId,
      locationId: AUTH.locationId
  );

  dynamic res;
  if (repo.getAccountStatus() != AccountStatus.isLoggedIn) {
    print("Trying to login");
    res = await repo.login("prueba", {"mLocationId" : AUTH.locationId});
    print("Login result: $res");
  }

  // todo: Strange workflow here:
  // 1. If you don't initRetroshare(), when login it try to create an identity
  // 2. If you login after initRetroshare(), the passphrase change for the location pass,
  // So you have to init retroshare again. Check retroshare.login() function, there
  // on memory password is set up again
  retroshare.initRetroshare(
      apiUser: AUTH.apiUser,
      passphrase: AUTH.passphrase,
      identityId: AUTH.identityId,
      locationId: AUTH.locationId
  );

  res = await retroshare.RsGxsForum.getForumsSummaries();
  print("Forum Summaries: $res");


}
